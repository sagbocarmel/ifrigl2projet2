package ds;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

public class FileDS {

    /**
     * dossier parent du fichier
     */
    private FileDS parentFolder;

    /**
     * nom du fichier ou dossier
     */
    private String  fileName;

    /**
     * date de modification du fichier
     */
    private Date  lastModify;

    /**
     * taille du fichier
     */
    private long fileSize;

    /**
     * chemin absolu du fichier
     */
    private String absoluteFilePath;

    /**
     * chemin absolu du dossier parent
     */
    private String absolutePath;

    /**
     * est à true si c'est un fichier
     */
    private boolean files;

    /**
     * est à true si c'est un repertoire
     */
    private boolean  directorys;

    /**
     * extension du fichier
     */
    private String extension;

    /**
     * tag du fichier
     */
    private String fileTag;


    public String getFileTag() {
        return fileTag;
    }

    public void setFileTag(String fileTag) {
        this.fileTag = fileTag;
    }

    /**
     * Liste de dossier et fichier contenu dans un repertoire de la structure
     */
    private ArrayList<FileDS > listFileIn;

    /**
     *
     * @param path chemin vers le fichier ou dossier
     * @param first signifie au cas où c'est un dossier s'il est le premier dossier ouvert sur la machine dans l'application
     */
    public FileDS(String path, boolean first)
    {

        File file = new File(path);
        System.out.println(file.getName());
        if(file.exists())
        {
            setFileName(file.getName());
            if(first == true)
            {
                parentFolder = null;
            }

            setLastModify(new Date(file.lastModified()));
            setFileSize(file.length());
            setAbsoluteFilePath(file.getAbsolutePath());
            setAbsolutePath(file.getPath());

            if(file.isFile())
            {
                setFiles(true);
                setListFileIn(null);
                setDirectorys(false);
                extension = fileName.substring(fileName.lastIndexOf('.'));
                fileTag = "File_"+extension.toUpperCase();
            }else
            {
                setFiles(false);
                listFileIn = new ArrayList<FileDS>();
            }
        }
    }

    public FileDS getParentFolder() {
        return parentFolder;
    }

    public void setParentFolder(FileDS parentFolder) {
        this.parentFolder = parentFolder;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Date getLastModify() {
        return lastModify;
    }

    public void setLastModify(Date lastModify) {
        this.lastModify = lastModify;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public String getAbsoluteFilePath() {
        return absoluteFilePath;
    }

    public void setAbsoluteFilePath(String absoluteFilePath) {
        this.absoluteFilePath = absoluteFilePath;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }

    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }

    public boolean isFiles() {
        return files;
    }

    public void setFiles(boolean files) {
        this.files = files;
    }

    public boolean isDirectorys() {
        return directorys;
    }

    public void setDirectorys(boolean directorys) {
        this.directorys = directorys;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public ArrayList<FileDS> getListFileIn() {
        return listFileIn;
    }

    public void setListFileIn(ArrayList<FileDS> listFileIn) {
        this.listFileIn = listFileIn;
    }

    /**
     * parcours un dossier et rempli sa suite
     */
    public void browseFolder()
    {
        if(!isFiles())
        {
            File fs = new File(getAbsoluteFilePath());
            for(File fis : fs.listFiles())
            {
                if(!fis.isFile())
                {
                    FileDS fl = new FileDS(fis.getAbsolutePath(),false);
                    fl.browseFolder();
                    fl.setParentFolder(this);
                    listFileIn.add(fl);
                }
                else
                {
                    FileDS fe = new FileDS(fis.getAbsolutePath(),false);
                    fe.setParentFolder(this);
                    listFileIn.add(fe);
                }
            }
        }
    }

}
