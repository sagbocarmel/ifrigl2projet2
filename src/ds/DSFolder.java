package ds;

/**
 * Contient un elemenet de la structure de donné defini. qui désigne la tete
 */
public class DSFolder {

    private FileDS headFolder;

    public DSFolder(String path) throws Exception {
        headFolder = new FileDS(path, true);
        if (headFolder.isFiles()) {
            throw new Exception("Not a folder");
        }
        headFolder.browseFolder();
    }

    public FileDS getHeadFolder() {
        return headFolder;
    }

    public void setHeadFolder(FileDS headFolder) {
        this.headFolder = headFolder;
    }
}
