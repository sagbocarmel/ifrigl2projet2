package gr;

import ds.DSFolder;
import ds.FileDS;
import sun.misc.Regexp;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Frame extends JFrame {
    private JTree tree;
    private DefaultMutableTreeNode parentNode;
    private ToolBars toolBars;
    private JLabel labelFolder;
    private JScrollPane paneFile;
    private JScrollPane paneTable;
    private JTable tableFile;
    private String[] title = {"File","File Path", "File Tag"};
    private Object[][] data = {
            {"", "", ""}
    };
    DSFolder folder;

    public Frame(){
        this.setSize(900, 600);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Tree DS");
        parentNode = new DefaultMutableTreeNode();
        toolBars = new ToolBars();
        labelFolder = new JLabel();
        tableFile = new JTable(data,title);

        this.setLayout(new BorderLayout());
        tree = new JTree(new DefaultMutableTreeNode());
        paneFile = new JScrollPane(tree);
        paneTable = new JScrollPane(tableFile);


        getContentPane().add(paneFile,BorderLayout.CENTER);
        getContentPane().add(paneTable,BorderLayout.EAST);
        toolBars.getButtonOpen().addActionListener(new ListenerButtonOpen());
        toolBars.getButtonTree().addActionListener(new ListenerButtonTree());
        toolBars.getButtonSearch().addActionListener(new ListenerButtonSearch());
        labelFolder.setText("NoFolder");
        this.getContentPane().add(toolBars,BorderLayout.NORTH);
        this.getContentPane().add(labelFolder,BorderLayout.SOUTH);
        this.setVisible(true);
    }

    /**
     * @param file
     * @param node
     * @return
     */
    private DefaultMutableTreeNode listFile(FileDS file, DefaultMutableTreeNode node){
        if(file.isFiles())
        {
            return new DefaultMutableTreeNode(file.getFileName());
        }
        else
        {
            ArrayList<FileDS> list = file.getListFileIn();
            if(list == null)
                return new DefaultMutableTreeNode(file.getFileName());
            for(FileDS nom : list){
                DefaultMutableTreeNode subNode;
                if(!nom.isFiles()){
                    subNode = new DefaultMutableTreeNode(nom.getFileName()+"/");
                    node.add(this.listFile(nom, subNode));
                }else{
                    subNode = new DefaultMutableTreeNode(nom.getFileName()+" #"+(nom.getFileTag()+"# "));
                }
                node.add(subNode);
            }
            return node;
        }
    }

    class ListenerButtonOpen implements ActionListener {
        JFileChooser chooser;
        String choosertitle = "Choisir un répertoire";
        @Override
        public void actionPerformed(ActionEvent e) {
            int result;

            chooser = new JFileChooser();
            chooser.setCurrentDirectory(new java.io.File("."));
            chooser.setDialogTitle(choosertitle);
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            chooser.setAcceptAllFileFilterUsed(false);
            if (chooser.showOpenDialog(toolBars) == JFileChooser.APPROVE_OPTION) {
                System.out.println("getSelectedFile() : "
                        +  chooser.getSelectedFile());
                labelFolder.setText(chooser.getSelectedFile().getAbsolutePath());
            }
            else {
                System.out.println("No Selection ");
            }
        }
    }

    class ListenerButtonTree implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                folder = new DSFolder(labelFolder.getText());
                parentNode = listFile(folder.getHeadFolder(),parentNode);
                tree = new JTree(parentNode);

                getContentPane().remove(paneFile);
                paneFile = new JScrollPane(tree);

                getContentPane().add(paneFile,BorderLayout.CENTER);

            }
            catch(Exception er)
            {
                er.printStackTrace();
            }
        }
    }

     class ListenerButtonSearch implements ActionListener
     {

        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println(toolBars.getTextField().getText());
            ArrayList<String[]> endResult = new ArrayList<String[]>();
            endResult = browseForSearch(folder.getHeadFolder(),toolBars.getTextField().getText(),endResult);
            Object[][] data = new String[endResult.size()][];
            for(int i = 0; i<endResult.size();i++)
            {
                data[i] = endResult.get(i);
            }
            tableFile = new JTable(data,title);

            getContentPane().remove(paneTable);
            paneTable = new JScrollPane(tableFile);
            getContentPane().add(paneTable,BorderLayout.EAST);

        }
    }

    /**
     * @param file
     * @param word
     * @param listresult
     * @return retourne la liste des fichiers retrouvés après recherche
     */
    private ArrayList<String[]> browseForSearch(FileDS file,String word,ArrayList<String[]> listresult)
    {
        if(file.isFiles())
        {
            if(file.getFileName().indexOf(word) !=0 || file.getFileTag().indexOf(word) !=0 )
            {
                String[] result = {file.getFileName(),file.getAbsolutePath(),file.getFileTag()};
                listresult.add(result);
                return listresult;
            }
        }
        else
        {
            ArrayList<FileDS> list = file.getListFileIn();
            if(list == null)
                return listresult;
            for(FileDS nom : list){

                if(!nom.isFiles())
                {
                    listresult = browseForSearch(nom,word,listresult);
                }
                else{
                    if(nom.getFileName().indexOf(word) !=0  || nom.getFileTag().indexOf(word) !=0 )
                    {
                        String[] result2 = {nom.getFileName(),nom.getAbsolutePath(),nom.getFileTag()};
                        System.out.println(listresult.size());
                        listresult.add(result2);
                    }
                }
            }

        }
        return listresult;
    }

}


