package gr;

import javax.swing.*;

public class ToolBars extends JToolBar {
    private JButton
            buttonOpen,
            buttonSearch,
            buttonTree;
    private JTextField textField;

    public ToolBars()
    {
        buttonOpen = new JButton("Open");
        buttonSearch = new JButton("Search");
        buttonTree = new JButton("Folder Tree");
        textField = new JTextField();
        add(buttonOpen);
        add(buttonTree);
        add(textField);
        add(buttonSearch);
    }

    public JButton getButtonOpen() {
        return buttonOpen;
    }

    public JButton getButtonSearch() {
        return buttonSearch;
    }

    public JButton getButtonTree() {
        return buttonTree;
    }

    public JTextField getTextField() {
        return textField;
    }
}
